var fs = require('fs');
var ethUtils = require('ethereumjs-util');

var walk = function(dir, done) {

  var results = [];

  var auxDir = dir

  fs.readdir(dir, function(err, list) {

    if (err) return done(err);
    var i = 0;

    (function next() {

      var file = list[i++];
      if (!file) return done(null, results);
      file = dir + '/' + file;

      fs.stat(file, function(err, stat) {

        if (stat && stat.isDirectory()) {

          walk(file, function(err, res) {

            results = results.concat(res);
            next();

          });

        } else {

          results.push(file);

          next();

        }

      });

    })();

  });

};

var hashCode = function(files, baseDir, done) {

    var hashedCode

    for (var i = 0; i < files.length; i++) {

      var code = fs.readFileSync(files[i]).toString()

      var filePath = files[i].substring(0 + baseDir.length, files[i].length)

      var currentCodeHash = ethUtils.bufferToHex(ethUtils.keccak256(filePath.toString() + code.toString()))

      if (hashedCode == undefined || hashedCode.length == 0) hashedCode = currentCodeHash

      else {

        hashedCode = ethUtils.bufferToHex(ethUtils.keccak256(hashedCode + currentCodeHash))

      }

    }

    return done(null, hashedCode);

}

module.exports = {

    walk,
    hashCode

}
